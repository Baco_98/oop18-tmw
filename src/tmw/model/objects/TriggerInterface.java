package tmw.model.objects;

/**
 * Interface that provides an activateTrigger method.
 */
@FunctionalInterface
public interface TriggerInterface {

    /**
     * Execute a specific command.
     */
    void activateTrigger();
}

