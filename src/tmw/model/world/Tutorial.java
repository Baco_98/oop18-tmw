package tmw.model.world;

import javafx.scene.shape.Rectangle;

/**
 * Class to represent the tutorial of the game.
 */
public class Tutorial extends AbstractWorld {

    /**
     * @param area The area of the tutorial
     */
    public Tutorial(final Rectangle area) {
        super(area);
    }
}

