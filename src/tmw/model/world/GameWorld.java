package tmw.model.world;

import java.util.List;
import java.util.Optional;

import javafx.geometry.Point2D;
import javafx.scene.shape.Rectangle;
import tmw.common.P2d;
import tmw.controller.entities.EntityController;
import tmw.controller.item.AbstractItemController;
import tmw.model.entities.GameEntity;
import tmw.model.item.Item;
import tmw.model.objects.GameObject;

/**
 * This class represents a gameWorld concept. World contains enemies, player,
 * items and bullets shoot by both enemies and player. All these entities are
 * closed in a gameWorldArea which represents the world size. There are multiple
 * methods which allows to interact with game entities. Allows also to be
 * observed by observers that'll be aware of changes.
 * 
 * @version 1.3
 */
public interface GameWorld {

    /**
     * This method check if entity it's placed in a valid position in world.
     * 
     * @param entity {@link GameEntity} entity reference
     * @return boolean response
     */
    default boolean checkInBounds(GameObject entity) {
        return (this.getWorldArea()
                .contains(new Point2D(entity.getCurrentPos().getX(), entity.getCurrentPos().getY())));
    }

    /**
     * Allows to populate world with entities.
     * 
     * @param entities {@link EntityController} entities with their positions
     *                 encapsulated
     * @param items    items
     */
    void populateWorld(List<? extends EntityController<?>> entities, List<AbstractItemController> items);

    /**
     * Insert item in world. Items have fixed position and must be specified because
     * items does not have any position value inside their classes.
     * 
     * @param obj item to insert
     */
    void insertItem(Item obj);

    /**
     * Removes item in world.
     * 
     * @param obj item to remove
     */
    void removeItem(Item obj);

    /**
     * Insert enemy in world.
     * 
     * @param enemy generic enemy
     */
    void insertEnemy(GameEntity enemy);

    /**
     * Removes enemy in world.
     * 
     * @param enemy generic enemy
     */
    void removeEnemy(GameEntity enemy);

    /**
     * Insert a bullet in world.
     * 
     * @param bullet bullet entity
     */
    void insertBullet(GameEntity bullet);

    /**
     * Removes bullet in world.
     * 
     * @param bullet entity
     */
    void removeBullet(GameEntity bullet);

    /**
     * Insert player in world.
     * 
     * @param player player entity
     */
    void insertPlayer(GameEntity player);

    /**
     * Removes the player. Represents player's death.
     * 
     * @param player entity
     */
    void killPlayer(GameEntity player);

    /**
     * Insert an obstacle (such as a simple wall) in the world.
     * 
     * @param obstacle obstacle entity to insert
     */
    void insertObstacle(GameObject obstacle);

    /**
     * Removes an obstacle from the world.
     *
     * @param obstacle to remove.
     */
    void removeObstacle(GameObject obstacle);

    /**
     * Gets enemy position.
     *
     * @param enemy enemy reference
     * @return position of that enemy
     */
    Optional<P2d> getEnemyPosition(GameEntity enemy);

    /**
     * Gets item position in world.
     * 
     * @param item {@link GameObject} item reference
     * @return item position
     */
    Optional<P2d> getItemPosition(Item item);

    /**
     * Gets player position in world.
     * 
     * @return An Optional of {@link P2d} that is the player position
     */
    Optional<P2d> getPlayerPosition();

    /**
     * Getter for world items.
     * 
     * @return list of items
     */
    List<Item> getItems();

    /**
     * Getter for enemies in world.
     * 
     * @return list of enemies
     */
    List<GameEntity> getEnemies();

    /**
     * Getter for all obstacles in world.
     * 
     * @return list of obstacles
     */
    List<GameObject> getObstacles();

    /**
     * Getter for current player in world.
     * 
     * @return An Optional of the player
     */
    Optional<GameEntity> getPlayer();

    /**
     * Returns the worldArea.
     * 
     * @return area represents world area
     */
    Rectangle getWorldArea();

    /**
     * Sets the new world area.
     * 
     * @param area new area
     */
    void setWorldArea(Rectangle area);
}
