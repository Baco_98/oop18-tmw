package tmw.view.level;

/**
 * Container for some events that user causes by interacting with game by view.
 */
public enum LevelViewEvents {

    /**
     * Zoom event.
     */
    ZOOM,

    /**
     * Fullscreen event.
     */
    FULLSCREEN,

    /**
     * Windowed event.
     */
    WINDOWED;
}

