package tmw.common;

/**
 * Enumeration that represent all the Game state that the videoGame can assume.
 *
 */
public enum GameStatesList {

    /**
     * Tutorial state.
     */
    TUTORIAL,

    /**
     * Level one state.
     */
    LEVEL1,

    /**
     * Boss level state.
     */
    LEVELBOSS,

    /**
     * Switch room state.
     */
    SWITCH_ROOM,

    /**
     * Tutorial finished.
     */
    TUTORIAL_DONE;

}

